const express = require('express');
const db = require('./db');
const utils = require('./utils');

const router = express.Router();
// //address
router.get('/address', (request, response) => {
    const connection = db.connect();
    const statement = `select id_add, street_name, landmark, city, pincode, state, id_registration from Address`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});


router.get('/address/:id', (request, response) => {
    const id = request.params.id;
    const connection = db.connect();
    const statement = `select id_add, street_name, landmark, city, pincode, state, id_registrationfrom Address where id_add = ${id_add}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});


router.post('/address', (request, response) => {
    const {street_name, landmark, city, pincode, state, id_registration} = request.body;
    const connection = db.connect();
    const statement = `insert into Address
            (street_name, landmark, city, pincode, state, id_registration ) values 
            ('${street_name}', '${landmark}', '${city}', '${pincode}', '${state}', '${id_registration}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/address/:id', (request, response) => {
    const id = request.params.id;
    const {street_name, landmark, city, pincode, state, id_registration} = request.body;
    const connection = db.connect();
    const statement = `update Address
        set
        street_name = '${street_name}',
        landmark = '${landmark}',
        city = '${city}',
            rating = '${rating}',
            pincode = '${pincode}',
            state = '${state}',
            id_registration = '${id_registration}',
        where id_add = ${id_add}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/address``/:id', (request, response) => {
    const id = request.params.id;
    const connection = db.connect();
    const statement = `delete from Address where id_add = ${id_add}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

//age
router.get('/age', (request, response) => {
    const connection = db.connect();
    const statement = `select id_age, age from Age`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});


router.get('/age/:id_age', (request, response) => {
    const id_age = request.params.id_age;
    const connection = db.connect();
    const statement = `select id_age, age from Age where id_age = ${id_age}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/age', (request, response) => {
    const { age } = request.body;
    const connection = db.connect();
    const statement = `insert into Age
            ( age ) values 
            ('${age}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/age/:id_age', (request, response) => {
    const id_age = request.params.id_age;
    const {age} = request.body;
    const connection = db.connect();
    const statement = `update Age
        set
        age = '${age}',
        where id_age = ${id_age}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
router.delete('/age/:id_age', (request, response) => {
    const id_age = request.params.id_age;
    const connection = db.connect();
    const statement = `delete from Age where id_age = ${id_age}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

//books
router.get('/books', (request, response) => {
    const connection = db.connect();
    const statement = `select id_book, b_name from Books`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/books/:id_book', (request, response) => {
    const id_book = request.params.id_book;
    const connection = db.connect();
    const statement = `select id_book, b_name from Books where id_book = ${id_book}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/books', (request, response) => {
    const {b_name} = request.body;
    const connection = db.connect();
    const statement = `insert into Books
            (b_name) values 
            ('${b_name}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/books/:id_book', (request, response) => {
    const id_book = request.params.id_book;
    const {b_name} = request.body;
    const connection = db.connect();
    const statement = `update Books
        set
            b_name = '${b_name}'
        where id_book = ${id_book}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/books/:id_book', (request, response) => {
    const id_book = request.params.id;
    const connection = db.connect();
    const statement = `delete from Books where id_book = ${id_book}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
//category
router.get('/category', (request, response) => {
    const connection = db.connect();
    const statement = `select id_cat, cat_name from Category`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/category/:id_cat', (request, response) => {
    const id_cat = request.params.id_cat;
    const connection = db.connect();
    const statement = `select id_cat,  cat_name from Category where id = ${id}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/category', (request, response) => {
    const {cat_name} = request.body;
    const connection = db.connect();
    const statement = `insert into Category
            (cat_name) values 
            ('${cat_name}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/category/:id_cat', (request, response) => {
    const id_cat = request.params.id_cat;
    const {cat_name} = request.body;
    const connection = db.connect();
    const statement = `update Category
        set
            cat_name = '${cat_name}'
            where id_cat = ${id_cat}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/category/:id_cat', (request, response) => {
    const id_cat = request.params.id_cat;
    const connection = db.connect();
    const statement = `delete from Category where id_cat = ${id_cat}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
//orderdetails

router.get('/order_detail/:id', (request, response) => {
    const id_add = request.params.id;
    const connection = db.connect();
    const statement = `select bill_no, pdt_name, brand_name, quantity, price, order_date, gst, total_amt, id_add, image  from Order_detail where id_add = ${id_add}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.get('/order_detail/', (request, response) => {
   //const id_add = request.params.id_add;
    const connection = db.connect();
    const statement = `select id_add,bill_no, pdt_name, brand_name, quantity, price, order_date, gst, total_amt, id_add, image  from Order_detail`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.post('/order_detail', (request, response) => {
    const { pdt_name, brand_name, quantity, price, order_date, gst, total_amt, id_add, image } = request.body;
    const connection = db.connect();
    const statement = `insert into Order_detail
            (pdt_name, brand_name, quantity, price, order_date, gst, total_amt, id_add, image ) values 
            ('${pdt_name}', '${brand_name}', '${quantity}', '${price}', '${order_date}', '${gst}', '${total_amt}', '${id_add}', ${image})`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});





router.put('/order_detail/:id', (request, response) => {
    const id_add = request.params.id;
    const {pdt_name, brand_name, quantity, price, order_date, gst, total_amt, image} = request.body;
    const connection = db.connect();
    const statement = `update Order_detail
        set
        pdt_name = '${pdt_name}',
        brand_name = '${brand_name}',
        quantity = '${quantity}',
        price = '${price}',
        order_date = '${order_date}',
        gst = '${gst}',
        total_amt = '${total_amt}',
        image = '${image}'
        where bill_no = ${bill_no}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/order_detail`/:id', (request, response) => {
    const id_add = request.params.id;
    const connection = db.connect();
    const statement = `delete from Order_detail where bill_no = ${bill_no}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
//product details
router.get('/product_detail', (request, response) => {
    const connection = db.connect();
    const statement = `select id_product, pdt_name, brand_name, price, quntity, description, image, id_vendor, id_cat from Product_detail`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/product_detail/:id_product', (request, response) => {
    const id_product = request.params.id_product;
    const connection = db.connect();
    const statement = `select id_product, pdt_name, brand_name, price, quntity, description, image, id_vendor, id_cat from Product_detail where id_product = ${id_product}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/product_detail', (request, response) => {
    const {pdt_name, brand_name, price, quntity, description, image, id_vendor, id_cat} = request.body;
    const connection = db.connect();
    const statement = `insert into Product_detail
            (pdt_name, brand_name, price, quntity, description, image, id_vendor, id_cat) values 
            ('${pdt_name}', '${brand_name}', '${price}', '${quntity}', '${description}', '${image}', '${id_vendor}', '${id_cat}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/product_detail/:id_product', (request, response) => {
    const id_product = request.params.id_product;
    const {pdt_name, brand_name, price, quntity, description, image, id_vendor, id_cat} = request.body;
    const connection = db.connect();
    const statement = `update Product_detail
        set
            pdt_name = '${pdt_name}',
            brand_name = '${brand_name}',
            price = '${price}',
            quntity = '${quntity}',
            directors = '${directors}',
            description = '${description}',
            image = '${image}',
            id_vendor = '${id_vendor}',
            id_cat = '${id_cat}'
        where id_product = ${id_product}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/product_detail/:id_product', (request, response) => {
    const id_product = request.params.id_product;
    const connection = db.connect();
    const statement = `delete from Product_detail where id_product = ${id_product}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
//registration
router.get('/registration', (request, response) => {
    const connection = db.connect();
    const statement = `select id_registration, first_name, last_name, phone_no, email, password from Registration`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});


router.get('/registration/:id_registration', (request, response) => {
    const id_registration = request.params.id_registration;
    const connection = db.connect();
    const statement = `select first_name, last_name, phone_no, email, password from Registration where id_registration = ${id_registration}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/registration', (request, response) => {
    const { first__name, last_name, phone_no, email, password} = request.body;
    const connection = db.connect();
    const statement = `insert into Registration
            ( first_name, last_name, phone_no, email, password) values 
            ('${first_name}', '${last_name}', '${phone_no}', '${email}', '${password}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/registration/:id', (request, response) => {
    const id_registration = request.params.id_registration;
    const { first_name, last_name, phone_no, email, password} = request.body;
    const connection = db.connect();
    const statement = `update Registration
        set
        first_name = '${first_name}',
        last_name = '${last_name}',
        phone_no = '${phone_no}',
        email = '${email}',
        password = '${password}',
        where id_registration = ${id_registration}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/registration/:id_registration', (request, response) => {
    const id_registration = request.params.id_registration;
    const connection = db.connect();
    const statement = `delete from Registration where id_registration = ${id_registration}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

//shopdetails
router.get('/shop_detail', (request, response) => {
    const connection = db.connect();
    const statement = `select id_shop, shop_name, shop_license, id_product from Shop_detail`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/shop_detail/:id_shop', (request, response) => {
    const id_shop = request.params.id_shop;
    const connection = db.connect();
    const statement = `select id_shop,  shop_name, shop_license, id_product from Shop_detail where id_shop = ${id_shop}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/shop_detail', (request, response) => {
    const {title, shortDescription, year, directors, writers, stars, genre, storyline} = request.body;
    const connection = db.connect();
    const statement = `insert into Shop_detail
            ( shop_name, shop_license, id_product) values 
            ('${shop_name}', '${shop_license}', '${id_product}'}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/shop_detail/:id_shop', (request, response) => {
    const id_shop = request.params.id_shop;
    const { shop_name, shop_license, id_product} = request.body;
    const connection = db.connect();
    const statement = `update Shop_detail
        set
            shop_name = '${shop_name}',
            shop_license = '${shop_license}',
            id_product = '${id_product}'
        where id_shop = ${id_shop}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/shop_detail/:id_shop', (request, response) => {
    const id_shop = request.params.id_shop;
    const connection = db.connect();
    const statement = `delete from Shop_detail where id_shop = ${id_shop}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
//vendore
router.get('/vendor', (request, response) => {
    const connection = db.connect();
    const statement = `select id_Vendor, id_registration, id_shop from Vendor`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.get('/vendor/:id_Vendor', (request, response) => {
    const id_Vendor = request.params.id_Vendor;
    const connection = db.connect();
    const statement = `select id_Vendor, id_registration, id_shop from Vendor where id_Vendor = ${id_Vendor}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result[0]));
    })
});

router.post('/vendor', (request, response) => {
    const { id_registration, id_shop } = request.body;
    const connection = db.connect();
    const statement = `insert into Vendor
            (id_registration, id_shop) values 
            ('${id_registration}', '${id_shop}')`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.put('/vendor/:id_Vendor', (request, response) => {
    const id_Vendor = request.params.id_Vendor;
    const {id_registration, id_shop} = request.body;
    const connection = db.connect();
    const statement = `update Vendor
        set
        id_registration = '${id_registration}',
        id_shop = '${id_shop}',
        where id_Vendor = ${id_Vendor}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});

router.delete('/vendor/:id_Vendor', (request, response) => {
    const id_Vendor = request.params.id_Vendor;
    const connection = db.connect();
    const statement = `delete from Vendor where id_Vendor = ${id_Vendor}`;
    connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createResponse(error, result));
    })
});
module.exports=router;